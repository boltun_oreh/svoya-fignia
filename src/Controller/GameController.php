<?php

namespace App\Controller;

use App\Entity\Game;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     * @Template("index.html.twig")
     */
    public function index()
    {
        $games = $this->getDoctrine()->getRepository(Game::class)->findBy([
                'enabled' => true,
            ],
            [
                'sortingOrder' => 'ASC',
            ]
        );

        return [
            'games' => $games,
        ];
    }

    /**
     * @Route("/games/{slug}/", name="game")
     * @Method("GET")
     * @Template("game.html.twig")
     */
    public function game(Game $game)
    {
        return [
            'game' => $game,
        ];
    }
}
