<?php

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BaseAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('enabled')
            ->add('sortingOrder')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

        /**
         * @param ListMapper $listMapper
         */
        protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('enabled')
            ->add('sortingOrder')
            ->addIdentifier('createdAt')
            ->addIdentifier('updatedAt')
        ;
    }

        /**
         * @param FormMapper $formMapper
         */
        protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('enabled')
            ->add('sortingOrder')
            ->add('createdAt', null, [
                'disabled' => true,
            ])
            ->add('updatedAt', null, [
                'disabled' => true,
            ])
        ;
    }
}
