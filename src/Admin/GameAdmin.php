<?php

namespace App\Admin;


use Application\Sonata\MediaBundle\Entity\Media;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Form\Type\MediaType;

class GameAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('slug')
            ->add('author')
        ;

        parent::configureDatagridFilters($datagridMapper);
    }

        /**
         * @param ListMapper $listMapper
         */
        protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->addIdentifier('slug')
            ->add('author')
            ->addIdentifier('image')
        ;

        parent::configureListFields($listMapper);
    }

        /**
         * @param FormMapper $formMapper
         */
        protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('slug', null, [
                'disabled' => true,
            ])
            ->add('author')
            ->add(
                'image',
                MediaType::class,
                [
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'default',
                    'required' => false,
                ]
            )
            ->add('stages')
        ;

        parent::configureFormFields($formMapper);
    }
}
